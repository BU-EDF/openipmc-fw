diff --git a/CM7/Core/Src/dimm_gpios.c b/CM7/Core/Src/dimm_gpios.c
index 8fced74..5e2ea8a 100644
--- a/CM7/Core/Src/dimm_gpios.c
+++ b/CM7/Core/Src/dimm_gpios.c
@@ -94,6 +94,7 @@ void pyld_12v_en_set_state(pyld_12V_en_state_t state)
     GPIO_SET_STATE(RESET,EN_12V);
   }
 
+  mt_printf ("pyld_12v_en_set_state %d\n", state);
   // specific for OpenIPMC-HW v1.2
   mcp23s17_write_pin_value(&pyld_12v_en_io_pin, (uint8_t) state);
 }
diff --git a/CM7/Core/Src/freertos.c b/CM7/Core/Src/freertos.c
index 962137a..c1d59c5 100644
--- a/CM7/Core/Src/freertos.c
+++ b/CM7/Core/Src/freertos.c
@@ -47,7 +47,7 @@
 
 /* Private variables ---------------------------------------------------------*/
 /* USER CODE BEGIN Variables */
-extern h7uart_periph_t huart_cli;
+extern h7uart_periph_t huart_pyldi;
 /* USER CODE END Variables */
 
 /* Private function prototypes -----------------------------------------------*/
@@ -69,9 +69,9 @@ void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
    syslog_push_from_isr("FATAL", "STACK OVERFLOW", SYSFAC_KERNEL, SYSSEV_CRITICAL); // Task is identified by syslog automatically
 
    // Message also sent via UART for redundancy (more reliable, but less accessible).
-   h7uart_uart_tx(huart_cli,(uint8_t*)"STACK OVERFLOW in task: ", 24, 1000);
-   h7uart_uart_tx(huart_cli,(uint8_t*)pcTaskName, strlen((const char*)pcTaskName), 1000);
-   h7uart_uart_tx(huart_cli,(uint8_t*)"\n\r", 2, 1000);
+   h7uart_uart_tx(huart_pyldi,(uint8_t*)"STACK OVERFLOW in task: ", 24, 1000);
+   h7uart_uart_tx(huart_pyldi,(uint8_t*)pcTaskName, strlen((const char*)pcTaskName), 1000);
+   h7uart_uart_tx(huart_pyldi,(uint8_t*)"\n\r", 2, 1000);
 }
 
 /* USER CODE END 4 */
@@ -93,7 +93,7 @@ void vApplicationMallocFailedHook(void)
    syslog_push_from_isr("FATAL", "MALLOC FAILURE in FreeRTOS heap", SYSFAC_KERNEL, SYSSEV_CRITICAL);
 
    // Message also sent via UART for redundancy (more reliable, but less accessible).
-   h7uart_uart_tx(huart_cli,(uint8_t*)"MALLOC FAILURE in FreeRTOS heap\n\r", 33, 1000);
+   h7uart_uart_tx(huart_pyldi,(uint8_t*)"MALLOC FAILURE in FreeRTOS heap\n\r", 33, 1000);
 
 }
 /* USER CODE END 5 */
diff --git a/CM7/Core/Src/main.c b/CM7/Core/Src/main.c
index 72837ad..c1518e3 100644
--- a/CM7/Core/Src/main.c
+++ b/CM7/Core/Src/main.c
@@ -991,9 +991,9 @@ static void uart_cli_init( void )
 {
   h7uart_uart_ret_code_t ret;
 
-  huart_cli_config.rx_callback = cli_rx_callback;
+  huart_pyldi_config.rx_callback = cli_rx_callback;
 
-  ret =  h7uart_uart_init_by_config(huart_cli,&huart_cli_config);
+  ret =  h7uart_uart_init_by_config(huart_pyldi,&huart_pyldi_config);
 
   if (ret != H7UART_RET_CODE_OK )
     Error_Handler();
@@ -1058,7 +1058,7 @@ void cli_rx_callback(uint8_t* data ,uint32_t len)
 
   // Checks if there is Frame Error state in the transmission.
   // In UART normal mode, this error is an excessive noise in Rx/Tx lines.
-  if ( h7uart_get_state(huart_cli) == H7UART_FSM_STATE_ERROR_FE )
+  if ( h7uart_get_state(huart_pyldi) == H7UART_FSM_STATE_ERROR_FE )
     return;
 
 #if CLI_AVAILABLE_ON_UART
@@ -1116,7 +1116,7 @@ void _putchar(char character)
 // CLI via UART
 #if CLI_AVAILABLE_ON_UART
 
-  h7uart_uart_tx(huart_cli,(uint8_t*)(&character),1,1000);
+  h7uart_uart_tx(huart_pyldi,(uint8_t*)(&character),1,1000);
 #endif
 
   // CLI via telnet
diff --git a/CM7/Core/Src/mt_printf.c b/CM7/Core/Src/mt_printf.c
index 64b40a2..2a1abad 100644
--- a/CM7/Core/Src/mt_printf.c
+++ b/CM7/Core/Src/mt_printf.c
@@ -44,7 +44,7 @@ static SemaphoreHandle_t printf_mutex = NULL;
 extern RTC_HandleTypeDef hrtc;
 
 // UART Handle
-extern h7uart_periph_t huart_cli;
+extern h7uart_periph_t huart_pyldi;
 
 /*
  * Initializer.
@@ -121,7 +121,7 @@ int uart_printf(const char* format, ...)
   int ret = vsnprintf_(buffer, 128, format, args);
   va_end( args );
 
-  h7uart_uart_tx(huart_cli, buffer, strlen(buffer), 1000);
+  h7uart_uart_tx(huart_pyldi, buffer, strlen(buffer), 1000);
 
   return ret;
 }
